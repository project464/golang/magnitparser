FROM golang as build
#ENV CGO_ENABLED=0
RUN mkdir /app
ADD . /app
WORKDIR /app
RUN go build -o /parser .


FROM golang
WORKDIR /
COPY --from=build /parser /parser
COPY --from=build /app/templates /templates
EXPOSE 8080
CMD ["/parser"]