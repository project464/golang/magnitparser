package requester

import (
	"bytes"
	"encoding/json"
	"io"
	"log"
	"net/http"
)

// curl 'https://web-gateway.middle-api.magnit.ru/v3/goods'   -H 'authority: web-gateway.middle-api.magnit.ru'   -H 'accept: */*'   -H 'accept-language: ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7'   -H 'content-type: application/json'   -H 'dnt: 1'   -H 'origin: https://magnit.ru'   -H 'referer: https://magnit.ru/'   -H 'sec-ch-ua: "Chromium";v="116", "Not)A;Brand";v="24", "Google Chrome";v="116"'   -H 'sec-ch-ua-mobile: ?0'   -H 'sec-ch-ua-platform: "Windows"'   -H 'sec-fetch-dest: empty'   -H 'sec-fetch-mode: cors'   -H 'sec-fetch-site: same-site'   -H 'user-agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/116.0.0.0 Safari/537.36'   -H 'x-app-version: 0.1.0'   -H 'x-client-name: magnit'   -H 'x-device-id: 9ndvcpcosn'   -H 'x-device-platform: Web'   -H 'x-device-tag: disabled'   -H 'x-platform-version: window.navigator.userAgent'   --data-raw '{"categoryIDs":[],"includeForAdults":true,"onlyDiscount":false,"order":"desc","pagination":{"number":1,"size":1000},"shopType":"1","sortBy":"price","storeCodes":["172651"]}'   --compressed
const (
	//data      = "{\"categoryIDs\":[],\"includeForAdults\":true,\"onlyDiscount\":false,\"order\":\"desc\",\"pagination\":{\"number\":1,\"size\":36},\"shopType\":\"1\",\"sortBy\":\"price\",\"storeCodes\":[\"172651\"]}"
	storeCode = "172651"
)

type Payload struct {
	CategoryIDs      []any      `json:"categoryIDs"`
	IncludeForAdults bool       `json:"includeForAdults"`
	OnlyDiscount     bool       `json:"onlyDiscount"`
	Order            string     `json:"order"`
	Pagination       Pagination `json:"pagination"`
	ShopType         string     `json:"shopType"`
	SortBy           string     `json:"sortBy"`
	StoreCodes       []string   `json:"storeCodes"`
}
type Pagination struct {
	Number int `json:"number"`
	Size   int `json:"size"`
}

func (r *Requester) fillPayload(i int, order string) io.Reader {
	payload := Payload{
		CategoryIDs:      []any{},
		IncludeForAdults: true,
		OnlyDiscount:     false,
		Order:            order,
		Pagination: Pagination{
			Number: i,
			Size:   36,
		},
		ShopType:   "1",
		SortBy:     "price",
		StoreCodes: []string{storeCode},
	}

	payloadBytes, err := json.Marshal(payload)
	if err != nil {
		log.Fatal(err)
	}
	body := bytes.NewReader(payloadBytes)
	return body

}

func (r *Requester) getresponse(i int, order string) []byte {
	//reader := bytes.NewReader([]byte(data))
	//req, err := http.NewRequest("POST", "https://web-gateway.middle-api.magnit.ru/v3/goods", reader)
	resp := r.request(i, order)
	bodyText, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}
	//log.Println("bodytext:", string(bodyText))
	return bodyText
}

func (r *Requester) request(i int, order string) *http.Response {
	req, err := http.NewRequest("POST", "https://web-gateway.middle-api.magnit.ru/v3/goods", r.fillPayload(i, order))
	if err != nil {
		log.Fatal(err)
	}
	req.Header.Set("Authority", "web-gateway.middle-api.magnit.ru")
	req.Header.Set("Accept", "*/*")
	req.Header.Set("Accept-Language", "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7")
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Dnt", "1")
	req.Header.Set("Origin", "https://magnit.ru")
	req.Header.Set("Referer", "https://magnit.ru/")
	req.Header.Set("Sec-Ch-Ua", "\"Chromium\";v=\"116\", \"Not)A;Brand\";v=\"24\", \"Google Chrome\";v=\"116\"")
	req.Header.Set("Sec-Ch-Ua-Mobile", "?0")
	req.Header.Set("Sec-Ch-Ua-Platform", "\"Windows\"")
	req.Header.Set("Sec-Fetch-Dest", "empty")
	req.Header.Set("Sec-Fetch-Mode", "cors")
	req.Header.Set("Sec-Fetch-Site", "same-site")
	req.Header.Set("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/116.0.0.0 Safari/537.36")
	req.Header.Set("X-App-Version", "0.1.0")
	req.Header.Set("X-Client-Name", "magnit")
	req.Header.Set("X-Device-Id", "9ndvcpcosn")
	req.Header.Set("X-Device-Platform", "Web")
	req.Header.Set("X-Device-Tag", "disabled")
	req.Header.Set("X-Platform-Version", "window.navigator.userAgent")

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Fatal(err)
	}
	return resp
}
