package main

import (
	"log"
	"parser/database"
	"parser/requester"
	"parser/templater"
	"time"
)

var d = database.Database{}
var r = requester.Requester{}
var t = templater.Templater{}

func init() {
	d.InitDB()
	r.D = d
	t.D = d

}

func main() {
	// спинер
	//go utils.Spiner()

	//r.FillDatabase()
	//log.Println(len(ag.Good))
	//log.Println("ID:", stats[0].MerchantID)
	//log.Print("price:")
	//for _, stat := range stats {
	//	log.Print(stat.Date.Format("02.01.2006 15:04:05"), " - ", stat.Price)
	//}
	//d.GenerateData(1000149482)
	go fillDatabaseTicker()
	t.RunServer()

}

func fillDatabaseTicker() {
	for {
		r.FillDatabase()
		log.Println("waiting for 6 hours")
		time.Sleep(time.Hour * 6)
	}
}
