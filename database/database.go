package database

import (
	"errors"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
	"log"
	"time"
)

type Database struct {
	DB *gorm.DB
}

type DatabaseData struct {
	ID   string `json:"id" gorm:"primary_key,uniq"`
	Code string `json:"code"`
	Name string `json:"name"`
}

type Statistics struct {
	MerchantID string    `json:"merchantid"`
	Date       time.Time `json:"time"`
	Price      string    `json:"price"`
}

func (d *Database) InitDB() {
	err := errors.New("")
	d.DB, err = gorm.Open(sqlite.Open("./gorm.db"), &gorm.Config{})
	if err != nil {
		log.Fatal(err)
	}
	err = d.DB.AutoMigrate(&DatabaseData{}, &Statistics{})
	if err != nil {
		log.Fatal(err)
	}
}
