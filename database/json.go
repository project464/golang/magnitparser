package database

import (
	"encoding/json"
	"log"
	"os"
	"time"
)

func (d *Database) GenerateData(id int) {
	//var ids = []int{1000149482, 1000308608, 1000191553}
	// Сериализация в JSON
	type info struct {
		Date  time.Time `json:"date"`
		Price string    `json:"price"`
	}
	type Data struct {
		Merchantid string `json:"merchantid"`
		Info       []info `json:"info"`
	}
	var merchantid Data
	//for i := 0; i < 3; i++ {
	stats, name := d.SelectById(id)
	log.Println("name: ", name)
	//merchantid = append(merchantid, Data{name, []info{}})
	for _, val := range stats {
		merchantid.Info = append(merchantid.Info, info{val.Date, val.Price})
	}
	dataJSON, err := json.Marshal(merchantid)
	if err != nil {
		log.Fatal(err)
	}
	// Сохранение в файл "data.json"
	err = os.WriteFile("templates/data.json", dataJSON, 0644)
	if err != nil {
		log.Fatal(err)
	}
	//}
	log.Println("Data generated")

}
