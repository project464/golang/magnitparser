package database

func (d *Database) SelectById(id int) (stat []Statistics, name string) {
	d.DB.Find(&stat, id)
	var temp DatabaseData
	d.DB.Find(&temp, id)
	name = temp.Name
	return
}
func (d *Database) SelectAllMerch() (data []DatabaseData) {
	d.DB.Find(&data)
	return
}
