package templater

import (
	"encoding/json"
	"html/template"
	"log"
	"net/http"
	"parser/database"
	"sort"
	"strconv"
	"strings"
	"time"
)

type Templater struct {
	D database.Database
}

type PageData struct {
	Items []database.DatabaseData
}

func (t *Templater) RunServer() {
	http.HandleFunc("/", t.handleRoot)
	http.HandleFunc("/id/", t.handleID)
	http.HandleFunc("/data/", t.responseData)

	http.ListenAndServe(":8080", nil)
}

func (t *Templater) responseData(w http.ResponseWriter, r *http.Request) {
	path := r.URL.Path

	// Получите id из URL пути
	splitted := strings.Split(path, "/")
	id := splitted[len(splitted)-1]

	idInt, err := strconv.Atoi(id)
	if err != nil {
		log.Panic(err)
	}
	stats, name := t.D.SelectById(idInt)
	log.Println("Name: ", name)

	type info struct {
		Date  time.Time `json:"date"`
		Price string    `json:"price"`
	}
	type Data struct {
		MerchantName string `json:"merchantname"`
		Info         []info `json:"info"`
	}
	var merchantid Data
	merchantid.MerchantName = name
	log.Println("name: ", name)
	for _, val := range stats {
		merchantid.Info = append(merchantid.Info, info{val.Date, val.Price})
	}
	dataJSON, err := json.Marshal(merchantid)
	if err != nil {
		log.Println("Error marshaling JSON:", err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(dataJSON)

}

func (t *Templater) handleRoot(w http.ResponseWriter, r *http.Request) {
	mercs := t.D.SelectAllMerch()
	sort.Slice(mercs, func(i, j int) bool {
		return mercs[i].Name < mercs[j].Name
	})

	data := PageData{
		Items: mercs,
	}

	tmpl := template.Must(template.ParseFiles("templates/filter-test.html"))
	tmpl.Execute(w, data)
}

func (t *Templater) handleID(w http.ResponseWriter, r *http.Request) {
	path := r.URL.Path

	// Получите id из URL пути
	splitted := strings.Split(path, "/")
	id := splitted[len(splitted)-1]

	idInt, err := strconv.Atoi(id)
	if err != nil {
		log.Panic(err)
	}
	stats, name := t.D.SelectById(idInt)

	// Используйте id для подготовки данных
	log.Println("name: ", name)
	// Отправьте данные в ответ
	//fmt.Fprintf(w, "Данные для id %s: %v", name, stats)
	tmpl := template.Must(template.ParseFiles("templates/index-new.html"))
	tmpl.Execute(w, stats)
}
